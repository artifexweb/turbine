/*==========================================
============Start jquery elements===========
==========================================*/
(function($) {

	// Add under here any jquery elements
	$(document).ready(function(){

		$('#banner').owlCarousel({
			items : 1,
			loop: true,
			dots: true,
			nav: true,
			navText: [
				"<i class='fa fa-chevron-left'></i>",
				"<i class='fa fa-chevron-right'></i>"
			],
			responsiveClass:true,
			responsive:{
				0:{
					dots: false,
					nav: false,
				},
				800:{
					dots:true,
				},
			}
		});

		$('#header').owlCarousel({
			items : 4,
			loop: true,
			center: true,
			dots: false,
			nav: true,
			navText: [
				"<i class='fa fa-arrow-left'></i>",
				"<i class='fa fa-arrow-right'></i>"
			],
			responsiveClass:true,
			responsive:{
				0:{
					items:1,
				},
				600:{
					items:2,
				},
				800:{
					items:3,
				},
				1000:{
					items:4,
				}
			}
		});

	});


})( jQuery );