<?php
/**
 * The Single Single template file.
 *
 * This is the most generic template file in a WordPress theme
 * It is used to display a page when nothing more specific matches a query.
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @ThemeName : Monsarrat 2017
 * @ThemeURL : http://artifexweb.com
 * @ThemeDeveloper : ArtifexWeb Team
 * @Wordpress
 *
 */

get_header(); 
?>
	<div class="row" id="index" style="display: none;">This is single.php - format: <?php echo get_post_format();?></div>

	<?php // Show the selected frontpage content.
		if ( have_posts() ) : 

			while ( have_posts() ) : the_post();

				get_template_part( 'template-parts/post/content', get_post_format() );

			endwhile;

		else : ?>

	<?php endif; ?>


<?php get_footer(); ?>