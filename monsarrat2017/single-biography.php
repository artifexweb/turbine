<?php
/**
 * Template Name: Bio Page
 * @ThemeName : Monsarrat 2017
 * @ThemeURL : http://artifexweb.com
 * @ThemeDeveloper : ArtifexWeb Team
 * @Wordpress
 *
 */

get_header(); 
?>
	<div class="row" id="bio" style="display: none;">This is bio</div>

	<?php // Show the selected frontpage content.

		if ( have_posts() ) : 

			while ( have_posts() ) : the_post();

				get_template_part( 'template-parts/post/content', 'bio' );

			endwhile;

		else : ?>

	<?php endif; ?>

	<div class="row" id="contacto">

		<img src="<?php echo MONSARRAT_THEME_PATH; ?>/images/title-location.png" alt="Location" class="img-responsive pul-left" style="padding: 10px;">

		<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 col-lg-offset-3 col-md-offset-3 col-sm-offset-3">

			<h3>Location</h3>

			<?php 

			$contact_data = ot_get_option( 'contact_ribbon_info', false );

			if ( $contact_data ) :

				foreach ( $contact_data as $contact ) :

					if ( strtolower($contact['title']) == 'location' ) : ?>

					<p><span class="glyphicon glyphicon-map-marker" aria-hidden="true"></span> <?php echo $contact['contact_info_ribbon']; ?></p>

				<?php elseif ( strtolower($contact['title']) == 'phone' ) : ?>

					<p><span class="glyphicon glyphicon-earphone" aria-hidden="true"></span> <a href="tel:<?php echo $contact['contact_info_ribbon']; ?>"><?php echo $contact['contact_info_ribbon']; ?></a></p>

				<?php elseif ( strtolower($contact['title']) == 'e-mail' || strtolower($contact['title']) == 'email' ) : ?>

				<p><span class="glyphicon glyphicon-envelope" aria-hidden="true"></span> <a href="mailto:<?php echo $contact['contact_info_ribbon']; ?>"><?php echo $contact['contact_info_ribbon']; ?></a></p>

			<?php endif;

				endforeach;

			else : ?>

				<p><span class="glyphicon glyphicon-map-marker" aria-hidden="true"></span> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque mattis magna in cursus turpis</p>

				<p><span class="glyphicon glyphicon-earphone" aria-hidden="true"></span> <a href="tel:12345678">1234-5678</a></p>

				<p><span class="glyphicon glyphicon-envelope" aria-hidden="true"></span> <a href="mailto:#">info@online.com</a></p>

			<?php endif; ?>

		</div>

		<div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">

		<h3>Contact Form</h3>

		<?php 

		$cf7_id = ot_get_option( 'contact_form_7', false );

		if ( $cf7_id ) : 

			echo do_shortcode( '[contact-form-7 id="'.$cf7_id.'" title="Contact Us Form"]' );

		else : ?>

		<form>

			<label for="nombres" class="sr-only">Your Name</label><input class="form-control"  id="nombres" maxlength="40" name="nombres" size="20" type="text" placeholder="Your Name" /><br>

			<label for="email" class="sr-only">Your Email</label><input class="form-control"  id="email" maxlength="40" name="email" size="20" type="email" placeholder="E-mail" /><br>

			<label for="message" class="sr-only">Your Message</label><textarea placeholder="Message" class="form-control" rows="3" name="message" id="message"></textarea><br>

			<button type="submit" name="enviar" id="enviar" class="btn btn-azul">SEND</button>

		</form>

		<?php endif; ?>

		</div>

	</div>


<?php get_footer(); ?>