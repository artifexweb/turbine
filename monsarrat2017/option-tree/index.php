<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * It is used to display a page when nothing more specific matches a query.
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @ThemeName : Dee New
 * @ThemeURL : http://dee.ie
 *
 */
 
get_header(); 
include_once("navHeader.php");

?>

<section class="page_title">
	<div class="container"><h2 class="text-left"><?php wp_title(''); ?></h2></div>
</section>

<section class="page_breadcumb_wrap">
	<div class="container">
		<div class="row">
			<div class="col-xs-6">
				<div class="page_name">Our Latest Spicy Blog Posts</div>
			</div>
			<div class="col-xs-6">
				<div class="breadcrumbs" typeof="BreadcrumbList" vocab="http://schema.org/">
					<?php if(function_exists('bcn_display'))
					{
						bcn_display();
					}?>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="utility_area">
	<div class="container">
		<div class="row">
			<div class="col-md-6">
				<div class="subscribe_box">
					<h3>SUBSCRIBE TO OUR NEWSLETTER</h3>
					<?php echo do_shortcode('[mc4wp_form id="105"]'); ?>
				</div>
			</div>
			
			<div class="col-md-6">
				<div class="blog_category_filter_box">
					<h3>FILTER BY CATEGORY</h3>
					<form id="category-select" class="category-select blog_category_filter" action="<?php echo esc_url( home_url( '/' ) ); ?>" method="post">
						<div class="form-group">
							<?php wp_dropdown_categories( 'show_count=1&hierarchical=1' ); ?>
						</div>	
						<button type="submit" name="submit" class="blog_category_filter_submit round_hover" value="Filter" >Filter</button>
					</form>
				</div>
			</div>
			
		</div>
	</div>
</section>

<section class="blog_wrap">
	<div class="container">
		<div class="row">
			<div class="col-md-8">
				<?php get_template_part('post-excerpt');?>
			</div>
			
			<?php get_sidebar(); ?>
			
		</div>
	</div>
</section>

<?php
	include_once("footer.php");
?>