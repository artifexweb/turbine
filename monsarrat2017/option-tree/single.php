<?php
	get_header(); 
	include_once("navHeader.php");
?>

<section class="page_title">
	<div class="container"><h2 class="text-left"><?php wp_title(''); ?></h2></div>
</section>

<section class="page_breadcumb_wrap">
	<div class="container">
		<div class="row">
			<div class="col-xs-6">
				<div class="page_name"><?php wp_title(''); ?></div>
			</div>
			<div class="col-xs-6">
				<div class="breadcrumbs" typeof="BreadcrumbList" vocab="http://schema.org/">
					<?php if(function_exists('bcn_display'))
					{
						bcn_display();
					}?>
				</div>
			</div>
		</div>
	</div>
</section>

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

<?php if (function_exists('setPostViews')) { setPostViews(get_the_ID()); } ?>

<section class="single_blog">
	<div class="container">
		<div class="blog_post_img"><?php the_post_thumbnail(); ?></div>
		<div class="blog_post_detail">
			<h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
			<ul class="clearfix">
				<li><i class="fa fa-calendar"></i><?php the_time('M d-Y') ?></li>
				<li><i class="fa fa-user"></i><?php the_author_posts_link(); ?></li>
				<li><i class="fa fa-comments"></i> <?php comments_popup_link( 'No Comment', '1 Comment', '% Comment'); ?></li>
			</ul>
			<?php the_content(); ?>
		</div>
		<div id="authorarea">
			<?php echo get_avatar( get_the_author_meta( 'user_email' ), 70 ); ?>
			<h3>About <?php get_the_author(); ?></h3>
			<div class="authorinfo">
			<?php the_author_meta( 'description' ); ?>
			<a class="author-link" href="<?php echo esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ); ?>" rel="author">View all posts by <?php get_the_author(); ?> <span class="meta-nav">&rarr;</span></a>
		</div>
		</div>
		<div class="post_author_box">
			<h3>POSTED BY : <span class="post_author_name"><?php the_author_posts_link(); ?></span></h3>
			<div class="post_author_detail">
				<?php echo get_avatar( get_the_author_meta() ); ?>
				<p><?php the_author_meta( 'description' ); ?></p>
				<a class="author-link" href="<?php echo esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ); ?>" rel="author">View all posts by <?php get_the_author(); ?> <span class="meta-nav">&rarr;</span></a>
			</div>
		</div>
	</div>
	
<?php endwhile; endif; ?>

<?php if (function_exists('wp_pagenavi')) { wp_pagenavi(); } else { include('inc/navigation.php'); } ?>

</section>

<?php include_once('inc/related-post.php'); ?>

<?php get_footer(); ?>