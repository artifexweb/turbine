<?php
/**
 * Monsarrat theme 2017 functions
 */


/* caching some variables */
define( 'MONSARRAT_THEME_PATH', get_template_directory_uri() );


function monsarrat_setup() {

	/**
	 * Make theme available for translation.
	 */

	load_theme_textdomain( 'monsarrat', get_template_directory() . '/languages' );

	 /**
	 * Support title tag and wordpage can generate title
	 */
	add_theme_support( 'title-tag' );

	 /**
	 * Custom image sizes for media upload and use
	 */

	include_once('inc/custom_image_sizes.php');
	
	 /*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );

	 /**
	 * Enable support for all Post Formats.
	 */
	add_theme_support( 'post-formats', array(
	//	'image',
	//	'quote',
		'gallery',
	) );


	 /**
	 * Dee New Theme support four menu and can extend
	 */
	register_nav_menus( array(

		'primary' => __( 'Primary Menu', 'monsarrat' ),
		'header' => __( 'Header Menu', 'monsarrat' ),
		'quick' => __( 'Quicklink Menu', 'monsarrat' ),

	) );

}

/**
*this function is hooked into the after_setup_theme hook, which runs before the init hook. 
*/
add_action( 'after_setup_theme', 'monsarrat_setup' );

//include custom posts on theme
include_once('inc/custom_posts.php');


//Enqueues scripts and styles
include_once('inc/enqueued_scripts.php');


/**
* Dee New Theme uses Option Tree Plugin in many area
* Theme Options Panel area
* Meta Box custom field in custom post area and more. 
* So be careful when removing option tree plugin from this theme
*/


/**
 * Required: set 'ot_theme_mode' filter to true.
 */
add_filter( 'ot_theme_mode', '__return_true' );



/**
 * Required: include OptionTree.
 */
require( trailingslashit( get_template_directory() ) . 'option-tree/ot-loader.php' );



/**
 * Required: OptionTree Theme Option
 */
require( trailingslashit( get_template_directory() ) . 'inc/theme-options.php' );



/**
 * include: DeeTheme custom meta-boxes with OptionTree
 */
include_once('inc/meta-boxes.php');



/**
 * include: DeeTheme custom admin styles
 */
add_action( 'admin_enqueue_scripts', 'load_admin_styles' );


function load_admin_styles() {
	wp_enqueue_style( 'admin_css', get_template_directory_uri() . '/css/admin-style.css', false, '1.0.0' );
}

/**
 * This is just to check if the post/page content is empty
 */
function empty_content($str) {
	return trim(str_replace('&nbsp;','',strip_tags($str))) == '';
}


remove_action('wp_head', 'wp_generator');



function numeric_posts_nav() {

	 global $wp_query;

	/** Stop execution if there's only 1 page */
	if( $wp_query->max_num_pages <= 1 )
		return;


	$paged = get_query_var( 'paged' ) ? absint( get_query_var( 'paged' ) ) : 1;
	$max   = intval( $wp_query->max_num_pages );

	/**	Add current page to the array */
	if ( $paged >= 1 )
		$links[] = $paged;

	/**	Add the pages around the current page to the array */
	if ( $paged >= 3 ) {
		$links[] = $paged - 1;
		$links[] = $paged - 2;
	}

	if ( ( $paged + 2 ) <= $max ) {
		$links[] = $paged + 2;
		$links[] = $paged + 1;
	}

	echo '<div class="navigation"><ul>' . "\n";

	/**	Previous Post Link */
	if ( get_previous_posts_link() )
		printf( '<li>%s</li>' . "\n", get_previous_posts_link( '&laquo; Previous', $max ) );

	/**	Link to first page, plus ellipses if necessary */
	if ( ! in_array( 1, $links ) ) {
		$class = 1 == $paged ? ' class="active"' : '';

		printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( 1 ) ), '1' );

		if ( ! in_array( 2, $links ) )
			echo '<li>…</li>';
	}

	/**	Link to current page, plus 2 pages in either direction if necessary */
	sort( $links );
	foreach ( (array) $links as $link ) {
		$class = $paged == $link ? ' class="active"' : '';
		printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( $link ) ), $link );
	}

	/**	Link to last page, plus ellipses if necessary */
	if ( ! in_array( $max, $links ) ) {
		if ( ! in_array( $max - 1, $links ) )
			echo '<li>…</li>' . "\n";

		$class = $paged == $max ? ' class="active"' : '';
		printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( $max ) ), $max );
	}

	/**	Next Post Link */
	if ( get_next_posts_link() )
		printf( '<li>%s</li>' . "\n", get_next_posts_link( 'Next &raquo;' ) );

	echo '</ul></div>' . "\n";

}

// Remove inline WordPress styles added with the gallery shortcode.
add_filter( 'use_default_gallery_style', '__return_false' );