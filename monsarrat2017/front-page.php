<?php
/**
 * Template Name: Homepage
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @ThemeName : Monsarrat 2017
 * @ThemeURL : http://artifexweb.com
 * @ThemeDeveloper : ArtifexWeb Team
 * @Wordpress
 *
 */
get_header(); 
?>
	<div class="row"><!-- ABOUT -->
		<?php // Show the selected frontpage content.
			if ( have_posts() ) :
				while ( have_posts() ) : the_post();
					get_template_part( 'template-parts/page/content', 'front-page' );
				endwhile;
			else : ?>
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
				<img src="<?php echo MONSARRAT_THEME_PATH; ?>/images/foto001.jpg" class="img-responsive center-block">
			</div>
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
			<img src="<?php echo MONSARRAT_THEME_PATH; ?>/images/title-01.png" class="img-responsive center-block">
			<h1 class="main-title">Welcome to our Techgage Website which provides information on Techgage in US.</h1>
			<br>
			<p class="texto">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque mattis magna erat, in cursus turpis hendrerit non. Quisque tempus id est quis lobortis. Duis eros tellus, venenatis ut neque nec, tincidunt varius ligula.</p>
			<a class="btn btn-azul" href="#">READ MORE</a>
		</div>
		<?php endif; ?>
	</div><!-- ABOUT FIN -->


<?php get_template_part( 'template-parts/page/content', 'page-loop' ) ?>


<?php get_footer(); ?>