<?php
/**
 * Displays content for front page
 *
 */

?>

<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">

	<br>
	<?php if ( has_post_thumbnail() ) :

		$imageUrl = get_the_post_thumbnail( $post->ID, 'full', array( 'class' => 'img-responsive center-block' ) );

		echo $imageUrl;

	endif; ?>

</div>

<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">

	<?php the_content(); ?>

</div>

