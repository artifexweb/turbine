<?php
/**
 * Template part for displaying blog post loop content
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @since 1.0
 * @version 1.0
 */

$paged = get_query_var( 'paged' ) ? absint( get_query_var( 'paged' ) ) : 1;
$posts_per_page = 10;
if ( is_front_page() ) $posts_per_page = 3;

$args = array( 
//	'category_name' => 'news',	// Uncomment if you want ONLY news cat to be displayed
	'numberposts' => -1,
	'posts_per_page' => $posts_per_page,
	'paged' => $paged
 );

$wp_query= new WP_Query( $args );

if ( $wp_query->have_posts() ) : ?>

		<?php
		if ( $above_news_ribbon_image = ot_get_option( 'above_news_ribbon_image', false )) : 
			echo '<div class="row mid-slogan">';
			if ( $above_news_ribbon_url = ot_get_option( 'above_news_ribbon_url', false )) : 
				echo '<a href="' . $above_news_ribbon_url .'">';
			endif;
			echo '<img src="' . $above_news_ribbon_image . '" class="above_news_ribbon_image">';
			if ( $above_news_ribbon_url) : 
				echo '</a>';
			endif;
			echo '</div>';
		endif; ?>


	<div class="row noticias" data-post-count="<?php echo count($wp_query); ?>"><!-- CONTENEDOR DE NOTICIAS -->

		<div id="checkup" style="display: none;">

			<?php print_r( $wp_query ); ?>

		</div>

		<?php
		if ( $custom_news_title_txt = ot_get_option( 'custom_news_title_txt', false )) : ?>
		<div class="col-lg-10 col-md-10 col-sm-10 col-xs-12 col-lg-offset-1 col-md-offset-1 col-sm-offset-1 news-section-title">
			<h1 class="custom_news_title_txt"><?php echo $custom_news_title_txt; ?></h1>
		</div>
		<?php else : ?>
		<div class="espacio-top"></div>
		<?php endif; ?>

<?php

	while ( $wp_query->have_posts() ) :

		setup_postdata( $wp_query );

		$wp_query->the_post();
		$postID = $wp_query->ID;

?> 
		<!-- DIV con Noticia -->
		<div class="col-lg-10 col-md-10 col-sm-10 col-xs-12 col-lg-offset-1 col-md-offset-1 col-sm-offset-1 noticia" id="<?php the_ID();?>">
			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 post-list-thumb">
				<br>
				<?php if ( has_post_thumbnail( ) ) :
					$imageUrl = get_the_post_thumbnail( get_the_ID(), 'post-thumbnail', array( 'class' => 'img-responsive center-block' ) );
					echo $imageUrl;
				endif; ?>
				<br>
			</div>
			<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12 post-list-content">
				<br>
				<?php the_title( '<h4 class="entry-title">', '</h4>' ); ?>
				<?php
				$summary = get_post_meta( get_the_ID(), 'page_custom_summary', true );

				if ( !empty( $summary ) ) echo $summary;
				else the_excerpt();

				echo '<a href="'.get_post_permalink(get_the_ID()).'" class="btn btn-amarillo pull-right">READ MORE</a>';
				?>
			</div>
		</div><!-- DIV con Noticia FIN -->

<?php endwhile; ?>


<?php 

if ( $wp_query->max_num_pages > 1 ) {
	echo '<div class="col-lg-10 col-md-10 col-sm-10 col-xs-12 col-lg-offset-1 col-md-offset-1 col-sm-offset-1 bottom-navigation">';

	$prev_link = get_previous_posts_link(__('&laquo; Older News'));
	$next_link = get_next_posts_link(__('Newer News &raquo;'), $wp_query->max_num_pages);

	if ( $prev_link || $next_link ) {

		if ( $posts_per_page == 3 )
			echo '<a href="/archives" class="btn btn-amarillo pull-right">SEE ALL</a>';
		else 
			numeric_posts_nav();
	}

	echo '</div>';
}

endif;

wp_reset_query();
?>

</div><!-- CONTENEDOR DE NOTICIAS FIN -->
