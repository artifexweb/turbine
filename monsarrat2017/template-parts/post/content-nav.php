<?php
/**
 * Template part for displaying navigational buttons
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @since 1.0
 * @version 1.0
 */

?>

<div class="row demoday navigation">

	<?php $prev = get_permalink(get_adjacent_post(false,'',false));

	$next = get_permalink(get_adjacent_post(false,'',true));

	if ($prev != get_permalink()) : ?>

		<a href="<?php echo $prev; ?>" class="btn btn-amarillo prev-button">Previous</a>

	<?php endif;

	if ($next != get_permalink()) : ?>

		<a href="<?php echo $next; ?>" class="btn btn-amarillo next-button">Next</a>

	<?php endif; ?>

</div>