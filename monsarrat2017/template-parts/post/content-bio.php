<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @since 1.0
 * @version 1.0
 */

?>

<div id="post-<?php the_ID(); ?>" class="row demoday">

	<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>

	<?php if ( $secondary_title = get_post_meta( get_the_ID(), 'secondary_title', true ) ) : 

		echo '<h2 class="entry-secondary_title">' . $secondary_title . '</h2>';

	endif; ?>

	<div class="title_separator"></div>

	<?php if ( $summary = get_post_meta( get_the_ID(), 'page_custom_summary', true ) ) : 

		echo '<p class="texto-summary">' . $summary . '</p>';

	endif; ?>

</div>

<div class="row demoday">

	<?php the_content(); ?>

</div>