<?php
/**
 * Initialize the custom theme options.
 */
add_action( 'init', 'custom_theme_options' );

/**
 * Build the custom settings & update OptionTree.
 */
function custom_theme_options() {
  
  /* OptionTree is not loaded yet, or this is not an admin request */
  if ( ! function_exists( 'ot_settings_id' ) || ! is_admin() )
	return false;
	
  /**
   * Get a copy of the saved settings array. 
   */
  $saved_settings = get_option( ot_settings_id(), array() );
  
  /**
   * Custom settings array that will eventually be 
   * passes to the OptionTree Settings API Class.
   */
  $custom_settings = array( 
	'contextual_help' => array( 
	  'content'       => array( 
		array(
		  'id'        => 'general_help',
		  'title'     => 'General',
		  'content'   => '<p>Help content goes here!</p>'
		)
	  ),
	  'sidebar'       => '<p>Sidebar content goes here!</p>'
	),
	'sections'        => array( 
	  array(
		'id'          => 'monsarrat_settings',
		'title'       => 'Monsarrat Settings',
		'tabs'        => array( 
		  array(
			'id'          => 'header_area',
			'title'       => 'Header'
		  ),
		  array(
			'id'          => 'front_page',
			'title'       => 'Front Page'
		  ),
		  array(
			'id'          => 'footer_area',
			'title'       => 'Footer'
		  )
		),
	  )
	),
	'settings'        => array( 
	  array(
		'id'          => 'JMLogo',
		'label'       => 'Update Monsarrat Logo',
		'desc'        => 'Upload new logo for Monsarrat theme.<br>Standard Logo Size : 171*164',
		'std'         => '',
		'type'        => 'upload',
		'section'     => 'monsarrat_settings'
	  ),
	  array(
		'id'          => 'top_ribbon_contact_info',
		'label'       => 'Change the contact phone and email in header',
		'desc'        => 'Modify the contact info',
		'std'         => '',
		'type'        => 'list-item',
		'section'     => 'monsarrat_settings',
		'settings'    => array( 
		  array(
			'id'          => 'contact_info_header',
			'label'       => __( 'Contact Data', 'theme-contact-info' ),
			'desc'        => 'Paste your contact data',
			'type'        => 'text',
			'class'       => '' )
		  )
	  ),
	  array(
		'id'          => 'social_links',
		'label'       => 'Add your social menu links',
		'desc'        => 'Update your social menu links here',
		'std'         => '',
		'type'        => 'list-item',
		'section'     => 'monsarrat_settings',
		'settings'    => array( 
		  array(
			'id'          => 'social_links_href',
			'label'       => __( 'Social address href', 'theme-text-domain' ),
			'desc'        => 'Paste your social id href',
			'type'        => 'text',
			'class'       => '' )
		  )
	  ),
	  array(
		'id'          => 'main_banner',
		'label'       => 'Add Images to the Main Banner',
		'desc'        => 'Upload the corresponding images',
		'std'         => '',
		'type'        => 'slider',
		'section'     => 'monsarrat_settings',
	  ),
	  array(
		'id'          => 'mini_slider',
		'label'       => 'Add Images to the Slider',
		'desc'        => 'Upload the corresponding images',
		'std'         => '',
		'type'        => 'slider',
		'section'     => 'monsarrat_settings'
	  ),
	  array(
		'id'          => 'above_news_ribbon_image',
		'label'       => 'Update Ribbon Image above News',
		'desc'        => 'Add here an image if you want a picture above the News section',
		'std'         => '',
		'type'        => 'upload',
		'section'     => 'monsarrat_settings'
	  ),
	  array(
		'id'          => 'above_news_ribbon_url',
		'label'       => 'Update Ribbon Image link above News',
		'desc'        => 'Add here an image if you want a link attached to the image above',
		'std'         => '',
		'type'        => 'text',
		'section'     => 'monsarrat_settings'
	  ),
	  array(
		'id'          => 'custom_news_title_txt',
		'label'       => 'Change the title of the News section',
		'desc'        => 'Change here if you want some personalized text for the News section',
		'std'         => '',
		'type'        => 'text',
		'section'     => 'monsarrat_settings'
	  ),
	  array(
		'id'          => 'contact_form_7',
		'label'       => 'Add a contact form to the footer',
		'desc'        => 'Add the CF7 ID to display a contact form in the footer',
		'std'         => '',
		'type'        => 'text',
		'section'     => 'monsarrat_settings',
	  ),
	  array(
		'id'          => 'left_footer_title',
		'label'       => 'Change the Title of the left footer section',
		'desc'        => 'Modify the footer-left title',
		'std'         => '',
		'type'        => 'text',
		'section'     => 'monsarrat_settings',
	  ),
	  array(
		'id'          => 'left_footer_text',
		'label'       => 'Change the Text in the left footer',
		'desc'        => 'Modify the footer-left text',
		'std'         => '',
		'type'        => 'textarea',
		'section'     => 'monsarrat_settings',
	  ),
	  array(
		'id'          => 'left_footer_url',
		'label'       => 'Change the link in the left footer',
		'desc'        => 'Modify the footer-left link',
		'std'         => '',
		'type'        => 'text',
		'section'     => 'monsarrat_settings',
	  ),
	  array(
		'id'          => 'left_footer_logo',
		'label'       => 'Change the Logo in the left footer',
		'desc'        => 'Modify the footer-left logo',
		'std'         => '',
		'type'        => 'upload',
		'section'     => 'monsarrat_settings',
	  ),
	  array(
		'id'          => 'left_footer_picture',
		'label'       => 'Change the small picture in the left footer',
		'desc'        => 'Modify the footer-left picture',
		'std'         => '',
		'type'        => 'upload',
		'section'     => 'monsarrat_settings',
		'class'       => 'ot-upload-attachment-id', // Optional CSS Class
	  ),
	  array(
		'id'          => 'right_footer_title',
		'label'       => 'Change the Title of the right footer section',
		'desc'        => 'Modify the footer-right title',
		'std'         => '',
		'type'        => 'text',
		'section'     => 'monsarrat_settings',
	  ),
	  array(
		'id'          => 'right_footer_text',
		'label'       => 'Change the Text in the right footer',
		'desc'        => 'Modify the footer-right text',
		'std'         => '',
		'type'        => 'textarea',
		'section'     => 'monsarrat_settings',
	  ),
	  array(
		'id'          => 'right_footer_url',
		'label'       => 'Change the link in the right footer',
		'desc'        => 'Modify the footer-right link',
		'std'         => '',
		'type'        => 'text',
		'section'     => 'monsarrat_settings',
	  ),
	  array(
		'id'          => 'right_footer_logo',
		'label'       => 'Change the Logo in the right footer',
		'desc'        => 'Modify the footer-right logo',
		'std'         => '',
		'type'        => 'upload',
		'section'     => 'monsarrat_settings',
	  ),
	  array(
		'id'          => 'right_footer_picture',
		'label'       => 'Change the small picture in the right footer',
		'desc'        => 'Modify the footer-right picture',
		'std'         => '',
		'type'        => 'upload',
		'section'     => 'monsarrat_settings',
		'class'       => 'ot-upload-attachment-id', // Optional CSS Class
	  ),
	  array(
		'id'          => 'footer_ribbon_contact_info',
		'label'       => 'Change the contact phone and email in footer',
		'desc'        => 'Modify the contact info',
		'std'         => '',
		'type'        => 'list-item',
		'section'     => 'monsarrat_settings',
		'settings'    => array( 
		  array(
			'id'          => 'contact_info_footer',
			'label'       => __( 'Contact Data', 'theme-contact-info' ),
			'desc'        => 'Paste your contact data',
			'type'        => 'text',
			'class'       => '' )
		  ),
	  ),
	  array(
		'id'          => 'quick_links',
		'label'       => 'Add quick links to the footer',
		'desc'        => 'Modify the quick links',
		'std'         => '',
		'type'        => 'list-item',
		'section'     => 'monsarrat_settings',
		'settings'    => array( 
		  array(
			'id'          => 'quick_link_url',
			'label'       => __( 'Contact Data', 'theme-contact-info' ),
			'desc'        => 'Paste your quick link',
			'type'        => 'text',
			'class'       => '' )
		  ),
	  ),
	  array(
		'id'          => 'change_the_copyright_text',
		'label'       => 'Change the copyright text in footer',
		'desc'        => 'Modify the bottom text of this site',
		'std'         => '',
		'type'        => 'text',
		'section'     => 'monsarrat_settings',
	  )
	)
  );
  
  /* allow settings to be filtered before saving */
  $custom_settings = apply_filters( ot_settings_id() . '_args', $custom_settings );
  
  /* settings are not the same update the DB */
  if ( $saved_settings !== $custom_settings ) {
	update_option( ot_settings_id(), $custom_settings ); 
  }
  
  /* Lets OptionTree know the UI Builder is being overridden */
  global $ot_has_custom_theme_options;
  $ot_has_custom_theme_options = true;
  
}