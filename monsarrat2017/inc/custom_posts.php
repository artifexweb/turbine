<?php
	/**
	 *Register Custom post : Bio
	 *Used in : about-us Page
	 */

	/**
	 *Register Custom Post : our_portfolios
	 *Used in : biography Page : All biography section
	 */
	function our_biography_add() {
		$labels = array(
			'name'               =>  __( 'Biographies' ),
			'singular_name'      => __( 'Biography' ),
			'add_new'            => 'Add New Biography',
			'add_new_item'            => 'Add New Biography',
			'new_item' => __( 'New Biography' ),
			'edit_item' => __( 'View Biography' ),
			'view_item' => __( 'View Biography' )
		);
		$args = array(
		  'public' => true,
		  'labels' => $labels,
		  'has_archive' => true,
		  'supports' => array( 'title', 'thumbnail'),
		  'menu_icon'   => 'dashicons-images-alt2',
		  /*'taxonomies' => array('category')*/
		);
		register_post_type('biography', $args);
	}
	add_action('init', 'our_biography_add');
	
	/**
	 * Register Custom Taxonomy : biographySection
	 ** Custom Post : biography
	 *Used in : biography Page : Biography section
	 */
	function custom_biography_taxonomy() {
		$labels = array(
			'name'                       => _x( 'BiographySections', 'Taxonomy General Name', 'text_domain' ),
			'singular_name'              => _x( 'BiographySection', 'Taxonomy Singular Name', 'text_domain' ),
			'menu_name'                  => __( 'Taxonomy', 'text_domain' ),
			'all_items'                  => __( 'All Biographies', 'text_domain' ),
			'parent_item'                => __( 'Parent BiographySections', 'text_domain' ),
			'parent_item_colon'          => __( 'Parent BiographySection:', 'text_domain' ),
			'new_item_name'              => __( 'New BiographySections', 'text_domain' ),
			'add_new_item'               => __( 'Add New BiographySections', 'text_domain' ),
			'edit_item'                  => __( 'Edit Item', 'text_domain' ),
			'update_item'                => __( 'Update Item', 'text_domain' ),
			'view_item'                  => __( 'View Item', 'text_domain' ),
			'separate_items_with_commas' => __( 'Separate Biography Sections with commas', 'text_domain' ),
		);
		$args = array(
			'labels'                     => $labels,
			'hierarchical'               => true,
		);
		register_taxonomy( 'biographySection', 'biography', $args );
	}
	add_action( 'init', 'custom_biography_taxonomy', 0 );