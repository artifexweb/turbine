<?php
	/**
	 * Custom image sizes added on the function by array merging
	 */
	if ( function_exists( 'add_image_size' ) ) {
		add_image_size( 'biography-slider', 660, 370, array( 'left', 'top' ) );
		add_image_size( 'blog-hero', 1100, 470, true );	//(cropped)
		add_image_size( 'blog-small', 83, 74, true ); 	//(cropped)
	}
	add_filter('image_size_names_choose', 'my_image_sizes');
	function my_image_sizes($sizes) {
		$addsizes = array(
			"biography-slider" => __( "Biography Slider", "monsarrat"),
			"blog-hero" => __( "Blog Hero", "monsarrat"),
			"blog-small" => __( "Blog Small", "monsarrat"),
		);
		$newsizes = array_merge($sizes, $addsizes);
		return $newsizes;
	}
	
	set_post_thumbnail_size( 285, 196, array( 'top', 'center')  ); // crop from the top and center horizontal
?>