<?php 
// The Query
$related = new WP_Query( array(
		'post_type' => 'post',
		'category__in' => wp_get_post_categories($post->ID),
		'posts_per_page' => 3,
		'post__not_in' => array($post->ID),
	)
 );
if( $related->have_posts() ) : ?>
	 <section class="you_may_like_wrap">
		<div class="container">
			<div class="you_may_like">
				<h3 class="you_may_like_title">YOU MAY LIKE</h3>
				<ul class="clearfix">
  <?php while( $related->have_posts() ) : $related->the_post(); ?>
		<li>
			<a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title(); ?>">
				<div class="img-related-thumb"><?php the_post_thumbnail( 'thumbnail' ); ?></div>
				<h3><?php the_title(); ?></h3>
				<p class="comment_count"><i class="fa fa-comments"></i><?php comments_number( 'No Comment', '1 Comment', '% Comment'); ?></p>
			</a>
		</li>
<?php endwhile;  ?>
				</ul>
			</div>
		</div>
	</section>
<?php endif; wp_reset_postdata(); ?>