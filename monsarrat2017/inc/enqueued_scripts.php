<?php

function monsarrat_scripts() {

	$version = '1.2';		// Avoids problems with CSS not displying properly when updating the stylesheet

	// Add bootstrap
	wp_enqueue_style( 'bootstrap', 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css', array(), '3.3.7', 'all' );
	wp_enqueue_style( 'bootstrap', 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css', array(), '3.3.7', 'all' );

	// Add font-awesome
	wp_enqueue_style( 'fontAwesome', 'https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css', array(), '4.7.0', 'all' );

	// Add owl-carousel
	wp_enqueue_style( 'owlCarousel', get_template_directory_uri() . '/css/owl.carousel.min.css', array(), '2.2.1', 'all' );

	// Add owl.theme
	wp_enqueue_style( 'owlTheme', get_template_directory_uri() . '/css/owl.theme.default.min.css', array(), '2.2.1', 'all' );

	// Add Google fonts Css
	wp_enqueue_style('g-fonts', 'https://fonts.googleapis.com/css?family=Lato:300,400,700,900|Roboto|Oswald|Krona+One', array(), '1.0', 'all');

	// Add deviceControl Css
	wp_enqueue_style( 'deviceControl', get_template_directory_uri() . '/css/device-control.css', array(), '1.1', 'all' );

	// Add customCss
	wp_enqueue_style( 'StylesLG', get_template_directory_uri() . '/css/styles.lg.css', array(), $version, 'all' );
	wp_enqueue_style( 'StylesSM', get_template_directory_uri() . '/css/styles.sm.css', array(), $version, 'all' );
	wp_enqueue_style( 'StylesXS', get_template_directory_uri() . '/css/styles.xs.css', array(), $version, 'all' );
	wp_enqueue_style( 'customCss', get_template_directory_uri() . '/style.css', array(), '1.1', 'all' );


	/**
	* adding js scripts for dee_new theme
	*/

	// Add jquery
	//wp_enqueue_script('jquery');

	if (!is_admin()) {
		wp_deregister_script('jquery');
		wp_register_script('jquery', 'https://code.jquery.com/jquery-1.12.4.min.js', false, '1.12.4');
		wp_enqueue_script('jquery');
	}

	// Add bootstrap_Js
//	wp_enqueue_script( 'bootstrap_Js', get_template_directory_uri() . '/js/bootstrap.min.js', array('jquery'), '3.3.4', 'true' );
	wp_enqueue_script( 'bootstrap_Js', 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js', array('jquery'), '3.3.7', 'true' );

	// Add Bootstrap_sliding_tab
	wp_enqueue_script( 'bootstrap_sliding_tab', get_template_directory_uri() . '/js/Bootstrap_sliding_tab.js', array('jquery'), 'false', 'true' );

	// Add jquery_matchHeight
	wp_enqueue_script( 'jquery_matchHeight', get_template_directory_uri() . '/js/jquery.matchHeight-min.js', array('jquery'), '0.5.2', 'true' );

	// Add owl_carousel_js
	wp_enqueue_script( 'owl_carousel_js', get_template_directory_uri() . '/js/owl.carousel.min.js', array('jquery'), '1.3.3', 'true' );

	// Add Custom Theme Script
	wp_enqueue_script( 'customScript', get_template_directory_uri() . '/js/script.js', array('jquery'), 'false', 'true' );

}

add_action( 'wp_enqueue_scripts', 'monsarrat_scripts' );
