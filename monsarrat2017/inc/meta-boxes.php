<?php
/**
 * Initialize the custom Meta Boxes. 
 */
add_action( 'admin_init', 'custom_meta_boxes' );

/**
 * Meta Boxes demo code.
 *
 * You can find all the available option types in demo-theme-options.php.
 *
 * @return    void
 * @since     2.0
 */
function custom_meta_boxes() {
	
	 /**
	* Create a custom meta boxes array that we pass to 
	* the OptionTree Meta Box API Class.
	*/
	$biography_page_meta_box = array(
		'id'          => 'biography_page_meta_box',
		'title'       => __( 'Biography Images', 'monsarrat' ),
		'desc'        => '',
		'pages'       => array( 'biography' ),
		'context'     => 'normal',
		'priority'    => 'high',
		'fields'      => array(
		  array(
			'label'       => __( 'Gallery', 'monsarrat' ),
			'id'          => 'biography_pro_gallery',
			'type'        => 'gallery',
			'desc'        => sprintf( __( 'Add this project images.<br>Dont forget to choose image size biographySlider for this gallery making', 'monsarrat' ), '<code>demo_show_gallery:is(on)</code>' ),
		   /* 'condition'   => 'demo_show_gallery:is(on)'*/
		  ),
		  array(
			'label'       => __( 'Biography Client', 'monsarrat' ),
			'id'          => 'biography_client',
			'type'        => 'text',
			'desc'        => __( 'The Client name', 'theme-text-domain' )
		  ),
		  array(
				'label'       => __( 'Bio Picture', 'monsarrat' ),
				'id'          => 'biography_picture',
				'type'        => 'upload',
				'desc'        => __( 'Upload a picture to show on the footer panel', 'deenew' )
			),
		  array(
			'label'       => __( 'Project Descripton', 'monsarrat' ),
			'id'          => 'biography_description',
			'type'        => 'textarea',
			'desc'        => __( 'Short Description of this project', 'monsarrat' )
		  ),
		)
	);

	/* Building block arrays for the custom metaboxes in pages */
	$scondary_title = array(
		'label'       => __( 'Secondary Title', 'monsarrat' ),
		'id'          => 'secondary_title',
		'type'        => 'text',
		'desc'        => __( 'In case of need of a secondary title for posts & pages', 'monsarrat' )
	);

	$main_summary_text = array(
		'label'       => __( 'Summary text', 'monsarrat' ),
		'id'          => 'page_custom_summary',
		'type'        => 'textarea',
		'desc'        => __( 'This is the text that goes above the post', 'monsarrat' )
	);

	/* Building block array for the CF7 */
	$custom_contact_form = array(
		'label'       => __( 'Form ID ', 'monsarrat' ),
		'id'          => 'cf7_id',
		'type'        => 'text',
		'desc'        => __( 'If the template shows a contact form, indicate the form ID corresponding to it', 'monsarrat' )
	);

	$posts_meta_box = array(
		'id'          => 'posts_meta_box',
		'title'       => __( 'Page Custom Options', 'monsarrat' ),
		'desc'        => '',
		'pages'       => array( 'post' ),
		'context'     => 'normal',
		'priority'    => 'high',
		'fields'      => array(
			$scondary_title,
			$main_summary_text
		)
	);

	$pages_meta_box = array(
		'id'          => 'pages_meta_box',
		'title'       => __( 'Page Custom Options', 'monsarrat' ),
		'desc'        => '',
		'pages'       => array( 'page' ),
		'context'     => 'normal',
		'priority'    => 'high',
		'fields'      => array(
			$scondary_title,
			$main_summary_text,
			$custom_contact_form
		)
	);


	$contact_us_meta_box = array(
		'id'          => 'contact_us_meta_box',
		'title'       => __( '[Contact Us] Page Custom Options', 'monsarrat' ),
		'desc'        => '',
		'pages'       => array( 'page' ),
		'context'     => 'normal',
		'priority'    => 'high',
		'fields'      => array(
			$main_summary_text
		)
	);
  
  /**
   * Register our meta boxes using the 
   * ot_register_meta_box() function.
   */
	if ( function_exists( 'ot_register_meta_box' ) ) {
		ot_register_meta_box( $biography_page_meta_box );
		ot_register_meta_box( $posts_meta_box );
		ot_register_meta_box( $pages_meta_box );

		if ( isset($_GET['post']) || isset($_POST['post_ID'])) {
			$post_id = isset($_GET['post']) ? $_GET['post'] : $_POST['post_ID'];
			$template_file = get_post_meta($post_id, '_wp_page_template', TRUE);

			if ( $template_file == 'contact_us.php' ) {
				ot_register_meta_box( $contact_us_meta_box );
			}
		}
	}
}