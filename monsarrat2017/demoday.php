<!DOCTYPE html>
  <html lang="en">
  <head>
  <title>Home</title>
	<META HTTP-EQUIV="Content-type" CONTENT="text/html; charset=UTF-8">
	<meta name="description" itemprop="description" content="">
	<meta name="keywords" itemprop="keywords" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" type="text/css" href="includes/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="css/styles.lg.css">
	<link rel="stylesheet" type="text/css" href="css/styles.sm.css">
	<link rel="stylesheet" type="text/css" href="css/styles.xs.css">
	<script src="includes/bootstrap/js/bootstrap.min.js"></script>
	<script src="includes/jquery-1.10.2.js"></script>
	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media 
	queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page 
	via file:// -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/
	html5shiv.js"></script>
	<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/
	respond.min.js"></script>
	<![endif]-->
  </head>
<body>
<!-- Container Top / Menu -->
<div class="container">
	<div class="row hidden-lg hidden-md hidden-sm" id="tope-xs">
		<div class="col-xs-6">
		  <div style="margin-top: 10px;">
			<a href="#"><img src="images/01-FB-top-xs.png" alt="Facebook" title="Facebook"></a>
			<a href="#"><img src="images/02-GP-top-xs.png" alt="Google+" title="Google+"></a>
			<a href="#"><img src="images/03-TW-top-xs.png" alt="Twitter" title="Twitter"></a>
			<a href="#"><img src="images/04-YT-top-xs.png" alt="Youtube" title="Youtube"></a>
		  </div>
		</div>
		<div class="col-xs-6">
		  <div style="margin-top: 10px;">
			<input type="text" name="search" placeholder="Search..." size="12">
		  </div>
		</div>
	</div>
	<div class="row" id="tope1"> <!-- Primera barra del tope -->
		<div class="logo hidden-xs"> <!-- DIV con Logo -->
			<a href="#" title="Home"><img src="images/logo.png" class="" alt="Logo"></a>
		</div><!-- DIV con Logo FIN -->
		<div class="col-lg-5 col-md-5 col-sm-5 col-xs-12 col-lg-offset-3 col-md-offset-3 col-sm-offset-3"><!-- DIV con NAVBAR -->
		<nav class="navbar navbar-default">
		  <div class="container-fluid">
		    <!-- Brand and toggle get grouped for better mobile display -->
		    <div class="navbar-header">
		      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
		        <span class="sr-only">Toggle navigation</span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		      </button>
		    </div>
		    <!-- Collect the nav links, forms, and other content for toggling -->
		    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
		      <ul class="nav navbar-nav">
		        <li><a href="#">About Me</a></li>
		        <li><a href="#">Demo Day</a></li>
		        <li><a href="#">Funding</a></li>
		      </ul>
		    </div><!-- /.navbar-collapse -->
		  </div><!-- /.container-fluid -->
		</nav>
		</div><!-- DIV con NAVBAR FIN -->
		<div class="col-lg-4 col-md-4 col-sm-4 hidden-xs"><!-- DIV con Social y Search -->
		  <div style="margin-top: 20px;">
			<a href="#"><img src="images/01-FB-top.png" alt="Facebook" title="Facebook"></a>
			<a href="#"><img src="images/02-GP-top.png" alt="Google+" title="Google+"></a>
			<a href="#"><img src="images/03-TW-top.png" alt="Twitter" title="Twitter"></a>
			<a href="#"><img src="images/04-YT-top.png" alt="Youtube" title="Youtube"></a>
			<input style="margin-left: 15px;" type="text" name="search" placeholder="Search...">
		  </div>
		</div><!-- DIV con Social y Search FIN -->

	</div> <!-- Primera barra del tope FIN -->
	<div class="row hidden-xs" id="tope2"><!-- Segunda barra del tope  -->
	</div><!-- Segunda barra del tope FIN -->
	<div class="row" id="banner"><!-- BANNER -->
		<img src="images/banner.jpg" class="img-responsive center-block" alt="Banner">
	</div><!-- BANNER FIN -->
	<div class="row" id="header"><!-- HEADER -->
		<img src="images/heading-holder.jpg" class="img-responsive center-block" alt="Heading">
	</div><!-- HEADER FIN -->	
	<div class="row demoday">
		<h1>Demo Day</h1>
		<p class="texto-demo">Before we spoke with any publishers, we gave "internal" demos to businesspeople friendly to the company. This is a fun story that I also include as a chapter in my forthcoming book, Soulburners. Johnny Monsarrat, Jeremy Gaffney, Tim Miller, and Toby Ragaini are featured.</p>
	</div>
	<div class="row demoday">
		<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
			<img src="images/demo-001.png" align="Demo Day" class="img-responsive center-block">
			<p class="texto-demo2">We learned a lot about running a business through preparing for these demos. Here we get salmon as a reward.</p>
			<img src="images/demo-004.png" align="Demo Day" class="img-responsive center-block">
			<p class="texto-demo2">We stuck to a "no demo code" rule, meaning that we would never write throw-away software just for a demo.</p>			
		</div>
		<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
			<img src="images/demo-002.png" align="Demo Day" class="img-responsive center-block">
			<p class="texto-demo2">Tim describes one of the technologies that gives our company an edge over the competition.</p>
			<img src="images/demo-005.png" align="Demo Day" class="img-responsive center-block">
			<p class="texto-demo2">We're working so hard, people start sleeping here.</p>			
		</div>
		<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
			<img src="images/demo-003.png" align="Demo Day" class="img-responsive center-block">
			<p class="texto-demo2">The demo of a man walking through a simple house, with the user interface laid out.</p>
			<img src="images/demo-006.png" align="Demo Day" class="img-responsive center-block">
			<p class="texto-demo2">Celebrating after the demo. Zach, Larry, Scott, Toby, Chris, Jeremy.</p>	
		</div>
	</div>
	<div class="row noticias"><!-- CONTENEDOR DE NOTICIAS -->
	<div class="espacio-top"></div>
		<!-- DIV con Noticia -->
		<div class="col-lg-10 col-md-10 col-sm-10 col-xs-12 col-lg-offset-1 col-md-offset-1 col-sm-offset-1 noticia">
			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
			<br>
				<img src="images/blog-001.jpg" alt="Blog" class="img-responsive center-block">
			</div>
			<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
			<br>
				<img src="images/calendar-icon.png" alt="Date"> 12/30/2015 <img src="images/comments-icon.png" alt="Comments"> 06 Comments
			<br>
			<h4>With artists Zach and Joe to integrate 3D art with the graphics engine</h4>
			<p class="texto-noticias">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque mattis magna erat, in cursus turpis hendrerit non. Quisque tempus id est quis lobortis. Duis eros tellus, venenatis ut neque nec, tincidunt varius ligula Nam sodales mollis....</p>
			<a href="#" class="btn btn-amarillo pull-right">READ MORE</a>
			</div>
		</div><!-- DIV con Noticia FIN -->
		<!-- DIV con Noticia -->
		<div class="col-lg-10 col-md-10 col-sm-10 col-xs-12 col-lg-offset-1 col-md-offset-1 col-sm-offset-1 noticia">
			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
			<br>
				<img src="images/blog-002.jpg" alt="Blog" class="img-responsive center-block">
			</div>
			<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
			<br>
				<img src="images/calendar-icon.png" alt="Date"> 12/30/2015 <img src="images/comments-icon.png" alt="Comments"> 06 Comments
			<br>
			<h4>Jeremy and Toby discuss game design.</h4>
			<p class="texto-noticias">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque mattis magna erat, in cursus turpis hendrerit non. Quisque tempus id est quis lobortis. Duis eros tellus, venenatis ut neque nec, tincidunt varius ligula Nam sodales mollis....</p>
			<a href="#" class="btn btn-amarillo pull-right">READ MORE</a>
			</div>
		</div><!-- DIV con Noticia FIN -->
		<!-- DIV con Noticia -->
		<div class="col-lg-10 col-md-10 col-sm-10 col-xs-12 col-lg-offset-1 col-md-offset-1 col-sm-offset-1 noticia">
			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
			<br>
				<img src="images/blog-003.jpg" alt="Blog" class="img-responsive center-block">
			</div>
			<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
			<br>
				<img src="images/calendar-icon.png" alt="Date"> 12/30/2015 <img src="images/comments-icon.png" alt="Comments"> 06 Comments
			<br>
			<h4>Doing an interview</h4>
			<p class="texto-noticias">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque mattis magna erat, in cursus turpis hendrerit non. Quisque tempus id est quis lobortis. Duis eros tellus, venenatis ut neque nec, tincidunt varius ligula Nam sodales mollis....</p>
			<a href="#" class="btn btn-amarillo pull-right">READ MORE</a>
			</div>
		</div><!-- DIV con Noticia FIN -->
		<!-- DIV con Noticia -->
		<div class="col-lg-10 col-md-10 col-sm-10 col-xs-12 col-lg-offset-1 col-md-offset-1 col-sm-offset-1 noticia">
			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
			<br>
				<img src="images/blog-004.jpg" alt="Blog" class="img-responsive center-block">
			</div>
			<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
			<br>
				<img src="images/calendar-icon.png" alt="Date"> 12/30/2015 <img src="images/comments-icon.png" alt="Comments"> 06 Comments
			<br>
			<h4>Jeremy, Chris, and Larry debugging the code.</h4>
			<p class="texto-noticias">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque mattis magna erat, in cursus turpis hendrerit non. Quisque tempus id est quis lobortis. Duis eros tellus, venenatis ut neque nec, tincidunt varius ligula Nam sodales mollis....</p>
			<a href="#" class="btn btn-amarillo pull-right">READ MORE</a>
			</div>
		</div><!-- DIV con Noticia FIN -->
		<!-- DIV con Noticia -->
		<div class="col-lg-10 col-md-10 col-sm-10 col-xs-12 col-lg-offset-1 col-md-offset-1 col-sm-offset-1 noticia">
			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
			<br>
				<img src="images/blog-005.jpg" alt="Blog" class="img-responsive center-block">
			</div>
			<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
			<br>
				<img src="images/calendar-icon.png" alt="Date"> 12/30/2015 <img src="images/comments-icon.png" alt="Comments"> 06 Comments
			<br>
			<h4>Chris Dyl working on the physics model</h4>
			<p class="texto-noticias">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque mattis magna erat, in cursus turpis hendrerit non. Quisque tempus id est quis lobortis. Duis eros tellus, venenatis ut neque nec, tincidunt varius ligula Nam sodales mollis....</p>
			<a href="#" class="btn btn-amarillo pull-right">READ MORE</a>
			</div>
		</div><!-- DIV con Noticia FIN -->
		<!-- DIV con Noticia -->
		<div class="col-lg-10 col-md-10 col-sm-10 col-xs-12 col-lg-offset-1 col-md-offset-1 col-sm-offset-1 noticia">
			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
			<br>
				<img src="images/blog-006.jpg" alt="Blog" class="img-responsive center-block">
			</div>
			<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
			<br>
				<img src="images/calendar-icon.png" alt="Date"> 12/30/2015 <img src="images/comments-icon.png" alt="Comments"> 06 Comments
			<br>
			<h4>Larry Skow, assembly programmer for graphics</h4>
			<p class="texto-noticias">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque mattis magna erat, in cursus turpis hendrerit non. Quisque tempus id est quis lobortis. Duis eros tellus, venenatis ut neque nec, tincidunt varius ligula Nam sodales mollis....</p>
			<a href="#" class="btn btn-amarillo pull-right">READ MORE</a>
			</div>
		</div><!-- DIV con Noticia FIN -->
		<!-- DIV con Noticia -->
		<div class="col-lg-10 col-md-10 col-sm-10 col-xs-12 col-lg-offset-1 col-md-offset-1 col-sm-offset-1 noticia">
			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
			<br>
				<img src="images/blog-007.jpg" alt="Blog" class="img-responsive center-block">
			</div>
			<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
			<br>
				<img src="images/calendar-icon.png" alt="Date"> 12/30/2015 <img src="images/comments-icon.png" alt="Comments"> 06 Comments
			<br>
			<h4>Jeremy writing the networking code</h4>
			<p class="texto-noticias">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque mattis magna erat, in cursus turpis hendrerit non. Quisque tempus id est quis lobortis. Duis eros tellus, venenatis ut neque nec, tincidunt varius ligula Nam sodales mollis....</p>
			<a href="#" class="btn btn-amarillo pull-right">READ MORE</a>
			</div>
		</div><!-- DIV con Noticia FIN -->
		<!-- DIV con Noticia -->
		<div class="col-lg-10 col-md-10 col-sm-10 col-xs-12 col-lg-offset-1 col-md-offset-1 col-sm-offset-1 noticia">
			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
			<br>
				<img src="images/blog-008.jpg" alt="Blog" class="img-responsive center-block">
			</div>
			<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
			<br>
				<img src="images/calendar-icon.png" alt="Date"> 12/30/2015 <img src="images/comments-icon.png" alt="Comments"> 06 Comments
			<br>
			<h4>Chris gives our first demo to outsiders</h4>
			<p class="texto-noticias">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque mattis magna erat, in cursus turpis hendrerit non. Quisque tempus id est quis lobortis. Duis eros tellus, venenatis ut neque nec, tincidunt varius ligula Nam sodales mollis....</p>
			<a href="#" class="btn btn-amarillo pull-right">READ MORE</a>
			</div>
		</div><!-- DIV con Noticia FIN -->
		<!-- DIV con Noticia -->
		<div class="col-lg-10 col-md-10 col-sm-10 col-xs-12 col-lg-offset-1 col-md-offset-1 col-sm-offset-1 noticia">
			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
			<br>
				<img src="images/blog-009.jpg" alt="Blog" class="img-responsive center-block">
			</div>
			<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
			<br>
				<img src="images/calendar-icon.png" alt="Date"> 12/30/2015 <img src="images/comments-icon.png" alt="Comments"> 06 Comments
			<br>
			<h4>Jeremy on the phone</h4>
			<p class="texto-noticias">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque mattis magna erat, in cursus turpis hendrerit non. Quisque tempus id est quis lobortis. Duis eros tellus, venenatis ut neque nec, tincidunt varius ligula Nam sodales mollis....</p>
			<a href="#" class="btn btn-amarillo pull-right">READ MORE</a>
			</div>
		</div><!-- DIV con Noticia FIN -->																
	</div><!-- CONTENEDOR DE NOTICIAS FIN -->
	<div class="row" id="contacto">
		<img src="images/title-location.png" alt="Location" class="img-responsive pul-left" style="padding: 10px;">
		<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 col-lg-offset-3 col-md-offset-3 col-sm-offset-3">
			<h3>Location</h3>
			<p><span class="glyphicon glyphicon-map-marker" aria-hidden="true"></span> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque mattis magna in cursus turpis</p>
			<p><span class="glyphicon glyphicon-earphone" aria-hidden="true"></span> <a href="tel:12345678">1234-5678</a></p>
			<p><span class="glyphicon glyphicon-envelope" aria-hidden="true"></span> <a href="mailto:#">info@online.com</a></p>
		</div>
		<div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">
		<h3>Contact Form</h3>
		<form>
			<label for="nombres" class="sr-only">Your Name</label><input class="form-control"  id="nombres" maxlength="40" name="nombres" size="20" type="text" placeholder="Your Name" /><br>
			<label for="email" class="sr-only">Your Email</label><input class="form-control"  id="email" maxlength="40" name="email" size="20" type="email" placeholder="E-mail" /><br>
			<label for="message" class="sr-only">Your Message</label><textarea placeholder="Message" class="form-control" rows="3" name="message" id="message"></textarea><br>
			<button type="submit" name="enviar" id="enviar" class="btn btn-azul">SEND</button>
		</form>
		</div>
	</div>
	<div class="row separador hidden-xs">
		<div class="col-lg-4 col-md-4 col-sm-4 col-lg-offset-1 col-md-offset-1">
			<br>
			<p style="color: white; font-size: 20px; font-family: Roboto;"><img src="images/phone-icon.png" alt="Phone"> Opening Hours : Monday - Friday</p>
		</div>
		<div class="col-lg-2 col-md-2 col-sm-2">
			<br><br>
			<h5 class="subtitle">The Turbine Story</h5>
		</div>
		<div class="col-lg-1 col-md-1 col-sm-1">
		<br>
			<img src="images/vertical.png" alt="" class="img-responsive center-block">
		</div>
		<div class="col-lg-4 col-md-4 col-sm-4">
		<br>
			<p class="texto-footer">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed tempor arcu non ligula convallis, vel tincidunt ipsum posuere. Fusce sodales lacus ut pellentesque sollicitudin duis.</p>
		</div>
	</div>
	<div class="row separador-xs hidden-lg hidden-md hidden-sm">
		<p style="padding:5px; text-align: center; color: white; font-size: 15px; font-family: Roboto;"><img src="images/phone-icon.png" alt="Phone"> Opening Hours : Monday - Friday</p>
	</div>
	<div class="row" id="foot"><!-- DIV row Footer -->
		<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 foot-first">
			<h5 class="subtitle-foot">The Turbine Story</h5>
			<p class="texto-foot">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed tempor arcu non ligula convallis, vel tincidunt ipsum posuere.</p>
			<p class="texto-foot">Fusce sodales lacus ut pellentesque sollicitudin duis.</p>
		</div>
		<div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
			<h5 class="subtitle-foot">Quick Link</h5>
			<p class="texto-foot"><span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span> <a href="#">Demo Day</a></p>
			<p class="texto-foot"><span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span> <a href="#">Funding</a></p>
		</div>
		<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
			<h5 class="subtitle-foot">About Us</h5>
			<table>
				<tr>
					<td><img src="images/foto001.png" alt="" class="img-responsive pull-left fotos-about"></td>
					<td><img src="images/foto002.png" alt="" class="img-responsive pull-left fotos-about"></td>
					<td><img src="images/foto003.png" alt="" class="img-responsive pull-left fotos-about"></td>
				</tr>
				<tr>
					<td><img src="images/foto004.png" alt="" class="img-responsive pull-left fotos-about"></td>
					<td><img src="images/foto005.png" alt="" class="img-responsive pull-left fotos-about"></td>
					<td><img src="images/foto006.png" alt="" class="img-responsive pull-left fotos-about"></td>
				</tr>				
			</table>
		</div>	
		<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
			<h5 class="subtitle-foot">Newsletter</h5>
			<p class="texto-foot">The weather started getting rough</p>
			<form>
				<label for="newsemail" class="sr-only">Your Name</label><input class="form-control form-newsletter"  id="newsemail" maxlength="40" name="newsemail" size="20" type="text" placeholder="" value="Email Address" /><br>
				<button type="submit" name="send" id="send" class="btn btn-newsletter">SUBSCRIBE NOW</button>
			</form>
			<div class="espacio-50"></div>
		</div>
	</div><!-- DIV row Footer FIN -->
	<div class="row" id="socket"><!-- DIV row Socket -->
		<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 col-lg-offset-4 col-md-offset-4 col-sm-offset-4">
		  <div style="text-align: center;">
			<a href="#"><img src="images/01-FB-bottom.png" alt="Facebook"></a>
			<a href="#"><img src="images/02-TW-bottom.png" alt="Twitter"></a>
			<a href="#"><img src="images/03-YT-bottom.png" alt="Youtube"></a>
			<a href="#"><img src="images/04-LN-bottom.png" alt="LinkedIn"></a>
		  </div>
		  <br>
		  <p class="texto-foot">Copyrights © 2017 <a href="#" style="color: #a56d15;">TECHCARE</a>. All Rights Reserved</p>
		</div>
	</div>
</div><!-- Container Main FIN -->
<script src="includes/bootstrap/js/bootstrap.min.js"></script>
</body>
</html>