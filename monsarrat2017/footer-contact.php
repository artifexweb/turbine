<?php
/**
 * The template for displaying Footer in Pages.
 *
 * @ThemeName : Monsarrat 2017
 * @ThemeURL : http://artifexweb.com
 * @ThemeDeveloper : ArtifexWeb Team
 * @Wordpress
 *
 */
?>

	<div class="row" id="contacto">
		<img src="<?php echo MONSARRAT_THEME_PATH; ?>/images/title-location.png" alt="Location" class="img-responsive pul-left" style="padding: 10px;">
		<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 col-lg-offset-3 col-md-offset-3 col-sm-offset-3">
			<h3>Location</h3>
			<?php 
			$contact_data = ot_get_option( 'contact_ribbon_info', false );

			if ( $contact_data ) :
				foreach ( $contact_data as $contact ) :
					if ( strtolower($contact['title']) == 'location' ) : ?>
					<p><span class="glyphicon glyphicon-map-marker" aria-hidden="true"></span> <?php echo $contact['contact_info_ribbon']; ?></p>
				<?php elseif ( strtolower($contact['title']) == 'phone' ) : ?>
					<p><span class="glyphicon glyphicon-earphone" aria-hidden="true"></span> <a href="tel:<?php echo $contact['contact_info_ribbon']; ?>"><?php echo $contact['contact_info_ribbon']; ?></a></p>
				<?php elseif ( strtolower($contact['title']) == 'e-mail' || strtolower($contact['title']) == 'email' ) : ?>
				<p><span class="glyphicon glyphicon-envelope" aria-hidden="true"></span> <a href="mailto:<?php echo $contact['contact_info_ribbon']; ?>"><?php echo $contact['contact_info_ribbon']; ?></a></p>
			<?php endif;
				endforeach;
			else : ?>
				<p><span class="glyphicon glyphicon-map-marker" aria-hidden="true"></span> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque mattis magna in cursus turpis</p>
				<p><span class="glyphicon glyphicon-earphone" aria-hidden="true"></span> <a href="tel:12345678">1234-5678</a></p>
				<p><span class="glyphicon glyphicon-envelope" aria-hidden="true"></span> <a href="mailto:#">info@online.com</a></p>
			<?php endif; ?>
		</div>
		<div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">
		<h3>Contact Form</h3>
		<?php 
		$cf7_id = ot_get_option( 'contact_form_7', false );
	//	$cf7_id = get_post_meta( get_the_ID(), 'cf7_id', true );
		if ( $cf7_id ) : 
			echo do_shortcode( '[contact-form-7 id="'.$cf7_id.'" title="Contact Us Form"]' );
		else : ?>
		<form>
			<label for="nombres" class="sr-only">Your Name</label><input class="form-control"  id="nombres" maxlength="40" name="nombres" size="20" type="text" placeholder="Your Name" /><br>
			<label for="email" class="sr-only">Your Email</label><input class="form-control"  id="email" maxlength="40" name="email" size="20" type="email" placeholder="E-mail" /><br>
			<label for="message" class="sr-only">Your Message</label><textarea placeholder="Message" class="form-control" rows="3" name="message" id="message"></textarea><br>
			<button type="submit" name="enviar" id="enviar" class="btn btn-azul">SEND</button>
		</form>
		<?php endif; ?>
		</div>
	</div>

	<div class="row separador hidden-xs">
		<div class="col-lg-4 col-md-4 col-sm-4 col-lg-offset-1 col-md-offset-1">
			<br>
			<p style="color: white; font-size: 20px; font-family: Roboto;"><img src="<?php echo MONSARRAT_THEME_PATH; ?>/images/phone-icon.png" alt="Phone"> Opening Hours : Monday - Friday</p>
		</div>
		<div class="col-lg-2 col-md-2 col-sm-2">
			<br><br>
			<h5 class="subtitle">The Turbine Story</h5>
		</div>
		<div class="col-lg-1 col-md-1 col-sm-1">
		<br>
			<img src="<?php echo MONSARRAT_THEME_PATH; ?>/images/vertical.png" alt="" class="img-responsive center-block">
		</div>
		<div class="col-lg-4 col-md-4 col-sm-4">
		<br>
			<?php
			$right_footer_quote = ot_get_option( 'right_footer_quote', false );
			if ( $right_footer_quote ) : ?>
			<p class="texto-footer"><?php echo $right_footer_quote; ?></p>
			<?php else : ?>
			<p class="texto-footer">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed tempor arcu non ligula convallis, vel tincidunt ipsum posuere. Fusce sodales lacus ut pellentesque sollicitudin duis.</p>
			<?php endif; ?>
		</div>
	</div>
	<div class="row separador-xs hidden-lg hidden-md hidden-sm">
		<p style="padding:5px; text-align: center; color: white; font-size: 15px; font-family: Roboto;"><img src="<?php echo MONSARRAT_THEME_PATH; ?>/images/phone-icon.png" alt="Phone"> Opening Hours : Monday - Friday</p>
	</div>
	<div class="row" id="foot"><!-- DIV row Footer -->
		<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 foot-first">
			<h5 class="subtitle-foot"><?php echo get_bloginfo('title'); ?></h5>
			<?php
			$left_footer_quote = ot_get_option( 'right_footer_quote', false );
			if ( $right_footer_quote ) : ?>
			<p class="texto-foot"><?php echo $right_footer_quote; ?></p>
			<?php else : ?>
			<p class="texto-foot">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed tempor arcu non ligula convallis, vel tincidunt ipsum posuere.</p>
			<p class="texto-foot">Fusce sodales lacus ut pellentesque sollicitudin duis.</p>
			<?php endif; ?>
		</div>
		<div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
			<h5 class="subtitle-foot">Quick Link</h5>
			<?php
			$quick_links = ot_get_option( 'quick_links', false );

			if ( $quick_links ) :

				foreach ( $quick_links as $link ) {
					echo '<p class="texto-foot"><span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span> <a href="'.$link['quick_link_url'].'">'.$link['title'].'</a></p>';
				}
			else : ?>
			<p class="texto-foot"><span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span> <a href="#">Demo Day</a></p>
			<p class="texto-foot"><span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span> <a href="#">Funding</a></p>
			<?php endif; ?>
		</div>
		<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
			<h5 class="subtitle-foot">About Us</h5>
			<?php
				// The Query
				$the_query = new WP_Query( array(
						'post_type' => 'biography',
						'posts_per_page' => 6,
						'order' => 'ASC',
					)
				 );

			if ( $the_query->have_posts() ) :
			// the loop
				while ( $the_query->have_posts() ) : $the_query->the_post();
						$biography_client = get_post_meta($post->ID, 'biography_client', true); 
						$biography_picture = get_post_meta($post->ID, 'biography_picture', true );
						$biography_pic_mini = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), "blog-small" );

					if ( !empty( $biography_picture ) ) : ?>
						<a class="item" href="<?php echo get_post_permalink($post->ID); ?>">
							<img src="<?php echo $biography_picture; ?>" class="biography_pic" alt="<?php echo $biography_client;?>">
						</a>
				<?php elseif ( !empty( $biography_pic_mini ) ) : ?>
						<a class="item" href="<?php echo get_post_permalink($post->ID); ?>">
							<img src="<?php echo $biography_pic_mini[0]; ?>" class="biography_pic" alt="<?php echo $biography_client;?>">
						</a>
					<?php endif; ?>
				<?php endwhile; ?>
			<?php else : ?>
			<table>
				<tr>
					<td><img src="<?php echo MONSARRAT_THEME_PATH; ?>/images/foto001.png" alt="" class="img-responsive pull-left fotos-about"></td>
					<td><img src="<?php echo MONSARRAT_THEME_PATH; ?>/images/foto002.png" alt="" class="img-responsive pull-left fotos-about"></td>
					<td><img src="<?php echo MONSARRAT_THEME_PATH; ?>/images/foto003.png" alt="" class="img-responsive pull-left fotos-about"></td>
				</tr>
				<tr>
					<td><img src="<?php echo MONSARRAT_THEME_PATH; ?>/images/foto004.png" alt="" class="img-responsive pull-left fotos-about"></td>
					<td><img src="<?php echo MONSARRAT_THEME_PATH; ?>/images/foto005.png" alt="" class="img-responsive pull-left fotos-about"></td>
					<td><img src="<?php echo MONSARRAT_THEME_PATH; ?>/images/foto006.png" alt="" class="img-responsive pull-left fotos-about"></td>
				</tr>
			</table>
			<?php endif; ?>
		</div>	
		<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
			<h5 class="subtitle-foot">Newsletter</h5>
			<p class="texto-foot">The weather started getting rough</p>
			<form>
				<label for="newsemail" class="sr-only">Your Name</label><input class="form-control form-newsletter"  id="newsemail" maxlength="40" name="newsemail" size="20" type="text" placeholder="" value="Email Address" /><br>
				<button type="submit" name="send" id="send" class="btn btn-newsletter">SUBSCRIBE NOW</button>
			</form>
			<div class="espacio-50"></div>
		</div>
	</div><!-- DIV row Footer FIN -->
	<div class="row" id="socket"><!-- DIV row Socket -->
		<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 col-lg-offset-4 col-md-offset-4 col-sm-offset-4">
		  <div style="text-align: center;">
		  			<?php 
			$social_links = ot_get_option( 'social_links', true );

			if ( $social_links ) :
				foreach ( $social_links as $social ) {
					if ( strtolower($social['title']) == 'facebook' ) $icon = '01-FB-bottom.png';
						else if ( strtolower($social['title']) == 'twitter' ) $icon = '02-TW-bottom.png';
						else if ( strtolower($social['title']) == 'youtube' ) $icon = '03-YT-bottom.png';
						else if ( strtolower($social['title']) == 'linkedin' ) $icon = '04-LN-bottom.png';
						else if ( strtolower($social['title']) == 'google+' ) $icon = '05-GP-bottom.png';

						echo '<a href="' . $social['social_links_href'] . '">';
						echo "<img src='";
						echo MONSARRAT_THEME_PATH;
						echo "/images/$icon'";
						echo ' alt="' . $social['title'] . '" title="' . $social['title'] . '"></a>';
				}
			else : ?>
			<a href="#"><img src="<?php echo MONSARRAT_THEME_PATH; ?>/images/01-FB-bottom.png" alt="Facebook"></a>
			<a href="#"><img src="<?php echo MONSARRAT_THEME_PATH; ?>/images/02-TW-bottom.png" alt="Twitter"></a>
			<a href="#"><img src="<?php echo MONSARRAT_THEME_PATH; ?>/images/03-YT-bottom.png" alt="Youtube"></a>
			<a href="#"><img src="<?php echo MONSARRAT_THEME_PATH; ?>/images/04-LN-bottom.png" alt="LinkedIn"></a>
			<?php endif ; ?>
		  </div>
		  <br>
		  <p class="texto-foot">Copyrights © 2017 <a href="http://linkedin.com/in/monsarrat/" style="color: #a56d15;">Johnny Monsarrat</a>. All Rights Reserved</p>
		</div>
	</div>

</div><!-- Container Main FIN -->

<?php wp_footer(); ?>

</body>
</html>