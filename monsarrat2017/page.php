<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * It is used to display a page when nothing more specific matches a query.
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @ThemeName : Monsarrat 2017
 * @ThemeURL : http://artifexweb.com
 * @ThemeDeveloper : ArtifexWeb Team
 * @Wordpress
 *
 */

get_header(); 
?>
	<div class="row" id="index" style="display: none;">This is index.php</div>
	<?php // Show the selected frontpage content.
		if ( have_posts() ) :
			while ( have_posts() ) : the_post();
				get_template_part( 'template-parts/page/content', 'page' );
			endwhile;
		else : ?>
	<div class="row demoday">
		<h1>Demo Day</h1>
		<p class="texto-demo">Before we spoke with any publishers, we gave "internal" demos to businesspeople friendly to the company. This is a fun story that I also include as a chapter in my forthcoming book, Soulburners. Johnny Monsarrat, Jeremy Gaffney, Tim Miller, and Toby Ragaini are featured.</p>
	</div>
	<div class="row demoday">
		<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
			<img src="images/demo-001.png" align="Demo Day" class="img-responsive center-block">
			<p class="texto-demo2">We learned a lot about running a business through preparing for these demos. Here we get salmon as a reward.</p>
			<img src="images/demo-004.png" align="Demo Day" class="img-responsive center-block">
			<p class="texto-demo2">We stuck to a "no demo code" rule, meaning that we would never write throw-away software just for a demo.</p>			
		</div>
		<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
			<img src="images/demo-002.png" align="Demo Day" class="img-responsive center-block">
			<p class="texto-demo2">Tim describes one of the technologies that gives our company an edge over the competition.</p>
			<img src="images/demo-005.png" align="Demo Day" class="img-responsive center-block">
			<p class="texto-demo2">We're working so hard, people start sleeping here.</p>			
		</div>
		<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
			<img src="images/demo-003.png" align="Demo Day" class="img-responsive center-block">
			<p class="texto-demo2">The demo of a man walking through a simple house, with the user interface laid out.</p>
			<img src="images/demo-006.png" align="Demo Day" class="img-responsive center-block">
			<p class="texto-demo2">Celebrating after the demo. Zach, Larry, Scott, Toby, Chris, Jeremy.</p>	
		</div>
	</div>
	<?php endif; ?>

<?php get_footer(); ?>