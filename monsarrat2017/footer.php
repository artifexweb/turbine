<?php
/**
 * The template for displaying Footer in Pages.
 *
 * @ThemeName : Monsarrat 2017
 * @ThemeURL : http://artifexweb.com
 * @ThemeDeveloper : ArtifexWeb Team
 * @Wordpress
 *
 */
?>

	<div class="espacio-bottom"></div>

	<div class="row" id="foot"><!-- DIV row Footer -->

		<div class="col-lg-6 col-md-6 col-sm-6 hidden-xs left-container">

			<div class="col-sm-6 foot-first left-side">

				<h5 class="subtitle-foot">
				<?php
				if ( $left_footer_url = ot_get_option( 'left_footer_url', false )) 
					echo '<a href="'.$left_footer_url.'">';

				if ( $left_footer_title = ot_get_option( 'left_footer_title', false )) :
					echo $left_footer_title;
				else :
					echo get_bloginfo('title');
				endif;

				if ( $left_footer_url ) echo '</a>'; ?>
				</h5>

				<?php
				if ( $left_footer_text = ot_get_option( 'left_footer_text', false )) : 
					echo $left_footer_text;
				else : ?>
					<p class="texto-foot">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed tempor arcu non ligula convallis, vel tincidunt ipsum posuere.</p>
				<?php endif; ?>

				<div class="image-logo-footer">
				<?php
				if ( $left_footer_logo = ot_get_option( 'left_footer_logo', false )) :
					if ( $left_footer_url ) echo '<a href="'.$left_footer_url.'">';

					echo '<img src="' . $left_footer_logo .'">';

					if ( $left_footer_url ) echo '</a>';
				endif; ?>
				</div>

			</div>

			<div class="col-sm-6 foot-first left-empty">

			<div class="image-picture-footer">

			<?php
			if ( $left_footer_picture_id = ot_get_option( 'left_footer_picture', false )) :
				$left_footer_picture = wp_get_attachment_image( $left_footer_picture_id, 'thumbnail' );
				echo $left_footer_picture;
			endif; ?>
			</div>

		</div>

		</div>

		<div class="col-lg-6 col-md-6 col-sm-6 hidden-xs right-container">

			<div class="col-sm-6 foot-first right-side">

				<h5 class="subtitle-foot">
				<?php
				if ( $right_footer_url = ot_get_option( 'right_footer_url', false )) 
					echo '<a href="'.$right_footer_url.'">';

				if ( $right_footer_title = ot_get_option( 'right_footer_title', false )) : 
					echo $right_footer_title;
				else : 
					echo get_bloginfo('title');
				endif;

				if ( $right_footer_url ) echo '</a>'; ?>
				</h5>

				<?php
				if ( $right_footer_text = ot_get_option( 'right_footer_text', false )) : 
					echo $right_footer_text;
				else : ?>
					<p class="texto-foot">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed tempor arcu non ligula convallis, vel tincidunt ipsum posuere.</p>
				<?php endif; ?>
				<div class="image-logo-footer">
				<?php
				if ( $right_footer_logo = ot_get_option( 'right_footer_logo', false )) :
					if ( $right_footer_url = ot_get_option( 'right_footer_url', false )) 
						echo '<a href="'.$right_footer_url.'">';

					echo '<img src="' . $right_footer_logo .'">';

					if ( $right_footer_url ) echo '</a>';
				endif; ?>
				</div>

			</div>

			<div class="col-sm-6  foot-first right-empty">

				<div class="image-picture-footer">

				<?php
				if ( $right_footer_picture_id = ot_get_option( 'right_footer_picture', false )) :
					$right_footer_picture = wp_get_attachment_image( $right_footer_picture_id, 'thumbnail' );
					echo $right_footer_picture;
				endif; ?>
				</div>

			</div>

		</div>

		<div class="hidden-lg hidden-md hidden-sm col-xs-12 foot-first responsive">

			<div class="col-xs-12 foot-first left-side">

				<h5 class="subtitle-foot">
				<?php
				if ( $left_footer_url ) 
					echo '<a href="'.$left_footer_url.'">';

				if ( $left_footer_title = ot_get_option( 'left_footer_title', false )) :
					echo $left_footer_title;
				else :
					echo get_bloginfo('title');
				endif;

				if ( $left_footer_url ) echo '</a>'; ?>
				</h5>

				<?php
				if ( $left_footer_text = ot_get_option( 'left_footer_text', false )) : 
					echo $left_footer_text;
				else : ?>
					<p class="texto-foot">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed tempor arcu non ligula convallis, vel tincidunt ipsum posuere.</p>
				<?php endif; ?>

				<div class="image-logo-footer center-block">

				<?php
				if ( $left_footer_logo = ot_get_option( 'left_footer_logo', false )) :
					if ( $left_footer_url ) 
						echo '<a href="'.$left_footer_url.'">';
					echo '<img src="' . $left_footer_logo .'">';
					if ( $left_footer_url ) echo '</a>';
				endif; ?>

				</div>

			</div>

			<div class="col-xs-12 foot-first center-block">

				<div class="image-picture-footer">

				<?php
				if ( $left_footer_picture_id = ot_get_option( 'left_footer_picture', false )) :
					$left_footer_picture = wp_get_attachment_image( $left_footer_picture_id, 'thumbnail' );
					echo $left_footer_picture;
				endif; ?>

				</div>

			</div>

			<div class="col-xs-12 footer-separator left-side"></div>

			<div class="col-xs-12 foot-first right-side">

				<h5 class="subtitle-foot">
				<?php
				if ( $right_footer_url ) 
					echo '<a href="'.$right_footer_url.'">';

				if ( $right_footer_title = ot_get_option( 'right_footer_title', false )) :
					echo $right_footer_title;
				else :
					echo get_bloginfo('title');
				endif;

				if ( $right_footer_url ) echo '</a>'; ?>
				</h5>

				<?php
				if ( $right_footer_text = ot_get_option( 'right_footer_text', false )) : 
					echo $right_footer_text;
				else : ?>
					<p class="texto-foot">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed tempor arcu non ligula convallis, vel tincidunt ipsum posuere.</p>
				<?php endif; ?>

				<div class="image-logo-footer center-block">

				<?php
				if ( $right_footer_logo = ot_get_option( 'right_footer_logo', false )) :
					if ( $right_footer_url ) 
						echo '<a href="'.$right_footer_url.'">';
					echo '<img src="' . $right_footer_logo .'">';
					if ( $right_footer_url ) echo '</a>';
				endif; ?>

				</div>

			</div>

			<div class="col-xs-12 foot-first center-block">

				<div class="image-picture-footer">
				<?php
				if ( $right_footer_picture_id = ot_get_option( 'right_footer_picture', false )) :
					$right_footer_picture = wp_get_attachment_image( $right_footer_picture_id, 'thumbnail' );
					echo $right_footer_picture;
				endif; ?>
				</div>

			</div>

			<div class="col-xs-12 footer-separator right-side"></div>

		</div>

	</div><!-- DIV row Footer FIN -->

	<div class="row" id="socket"><!-- DIV row Socket -->

		<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 col-lg-offset-4 col-md-offset-4 col-sm-offset-4">

			<div style="text-align: center;">
			<?php 
			$social_links = ot_get_option( 'social_links', true );

			if ( $social_links ) :
				foreach ( $social_links as $social ) :
					if ( strtolower($social['title']) == 'facebook' ) $icon = '01-FB-bottom.png';
					else if ( strtolower($social['title']) == 'twitter' ) $icon = '02-TW-bottom.png';
					else if ( strtolower($social['title']) == 'youtube' ) $icon = '03-YT-bottom.png';
					else if ( strtolower($social['title']) == 'linkedin' ) $icon = '04-LN-bottom.png';
					else if ( strtolower($social['title']) == 'google+' ) $icon = '05-GP-bottom.png';

					echo '<a href="' . $social['social_links_href'] . '">';
					echo "<img src='";
					echo MONSARRAT_THEME_PATH;
					echo "/images/$icon'";
					echo ' alt="' . $social['title'] . '" title="' . $social['title'] . '"></a>';
				endforeach;
			else : ?>
				<a href="#"><img src="<?php echo MONSARRAT_THEME_PATH; ?>/images/01-FB-bottom.png" alt="Facebook"></a>
				<a href="#"><img src="<?php echo MONSARRAT_THEME_PATH; ?>/images/02-TW-bottom.png" alt="Twitter"></a>
				<a href="#"><img src="<?php echo MONSARRAT_THEME_PATH; ?>/images/03-YT-bottom.png" alt="Youtube"></a>
				<a href="#"><img src="<?php echo MONSARRAT_THEME_PATH; ?>/images/04-LN-bottom.png" alt="LinkedIn"></a>
			<?php endif ; ?>
			</div>
			<br>
			<p class="texto-foot">Copyright © 2017 <a href="http://linkedin.com/in/monsarrat/" style="color: #a56d15;">Johnny Monsarrat</a>. All Rights Reserved</p>
		</div>
	</div>

</div><!-- Container Main FIN -->

<?php wp_footer(); ?>

</body>
</html>