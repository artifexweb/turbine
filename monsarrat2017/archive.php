<?php
/**
 * Template Name: Archives
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @ThemeName : Monsarrat 2017
 * @ThemeURL : http://artifexweb.com
 * @ThemeDeveloper : ArtifexWeb Team
 * @Wordpress
 *
 */
get_header(); 
?>

<?php get_template_part( 'template-parts/page/content', 'page-loop' ) ?>


<?php get_footer(); ?>