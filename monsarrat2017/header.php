<?php
/**
 * The template for displaying Header in Pages.
 *
 * @ThemeName : Monsarrat 2017
 * @ThemeURL : http://artifexweb.com
 * @ThemeDeveloper : ArtifexWeb Team
 * @Wordpress
 *
 */
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
	<head>
	<title><?php wp_title('|', true, 'right'); ?> <?php bloginfo('name'); ?></title>
		<meta charset="<?php bloginfo('charset'); ?>">
		<META HTTP-EQUIV="Content-type" CONTENT="text/html; charset=UTF-8">
		<meta name="description" itemprop="description" content="">
		<meta name="keywords" itemprop="keywords" content="">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media 
		queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page 
		via file:// -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/
		html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/
		respond.min.js"></script>
		<![endif]-->
		<?php wp_head(); ?>
	</head>
	<body <?php body_class(); ?>>

<!-- Container Top / Menu -->
<div class="container">
	<div class="row hidden-lg hidden-md hidden-sm" id="tope-xs">
		<div class="col-xs-6">
			<div style="margin-top: 10px;">
				<?php 
				$social_links = ot_get_option( 'social_links', true );

				if ( $social_links ) :
					foreach ( $social_links as $social ) {
						if ( strtolower($social['title']) == 'facebook' ) $icon = '01-FB-top-xs.png';
							else if ( strtolower($social['title']) == 'google+' ) $icon = '02-GP-top-xs.png';
							else if ( strtolower($social['title']) == 'twitter' ) $icon = '03-TW-top-xs.png';
							else if ( strtolower($social['title']) == 'youtube' ) $icon = '04-YT-top-xs.png';
							else if ( strtolower($social['title']) == 'linkedin' ) $icon = '05-LN-top-xs.png';

							echo '<a href="' . $social['social_links_href'] . '">';
							echo "<img src='";
							echo MONSARRAT_THEME_PATH;
							echo "/images/$icon'";
							echo ' alt="' . $social['title'] . '" title="' . $social['title'] . '"></a>';
					}
				else : ?>
				<a href="#"><img src="<?php echo MONSARRAT_THEME_PATH; ?>/images/01-FB-top-xs.png" alt="Facebook" title="Facebook"></a>
				<a href="#"><img src="<?php echo MONSARRAT_THEME_PATH; ?>/images/02-GP-top-xs.png" alt="Google+" title="Google+"></a>
				<a href="#"><img src="<?php echo MONSARRAT_THEME_PATH; ?>/images/03-TW-top-xs.png" alt="Twitter" title="Twitter"></a>
				<a href="#"><img src="<?php echo MONSARRAT_THEME_PATH; ?>/images/04-YT-top-xs.png" alt="Youtube" title="Youtube"></a>
				<?php endif ; ?>
			</div>
		</div>
		<div class="col-xs-6">
		  <div style="margin-top: 10px;">
			<input type="text" name="search" placeholder="Search..." size="12">
		  </div>
		</div>
	</div>

	<div class="row" id="tope1"> <!-- Primera barra del tope -->
		<div class="logo hidden-xs"> <!-- DIV con Logo -->
			<a href="<?php bloginfo('url'); ?>" title="Home">
			<?php 
			$theme_logo = ot_get_option( 'JMLogo', true );
			if ( $theme_logo ) : ?>
			<img src="<?php echo $theme_logo; ?>" class="" alt="Logo">
			<?php else :?>
			<img src="<?php echo MONSARRAT_THEME_PATH; ?>/images/logo.png" class="" alt="Logo">
			<?php endif; ?>
			</a>
		</div><!-- DIV con Logo FIN -->
		<div class="col-lg-5 col-md-5 col-sm-5 col-xs-12 col-lg-offset-3 col-md-offset-3 col-sm-offset-3"><!-- DIV con NAVBAR -->
			<nav class="navbar navbar-default">
				<div class="container-fluid">
					<!-- Brand and toggle get grouped for better mobile display -->
					<div class="navbar-header">
						<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
					</div>
					<?php

					if ( has_nav_menu( 'header' ) ) :
						$arg = array( 
							'theme_location'=> 'header', 
							'container_class' => 'collapse navbar-collapse', 
							'container_id' => 'bs-example-navbar-collapse-1',
							'menu_class' => 'nav navbar-nav' 
						);
						wp_nav_menu( $arg );
					else : ?>
					<!-- Collect the nav links, forms, and other content for toggling -->
					<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
						<ul class="nav navbar-nav">
							<li><a href="#">About Me</a></li>
							<li><a href="#">Demo Day</a></li>
							<li><a href="#">Funding</a></li>
						</ul>
					</div> 
					<!-- /.navbar-collapse -->
					<?php endif; ?>

				</div><!-- /.container-fluid -->
			</nav>
		</div><!-- DIV con NAVBAR FIN -->

		<div class="col-lg-4 col-md-4 col-sm-4 hidden-xs"><!-- DIV con Social y Search -->
			<div style="margin-top: 20px;">
			<?php $social_links = ot_get_option( 'social_links', true ); ?>
			<div style="display: none;">
				<?php print_r( $social_links );?>
			</div>
			<?php if ( $social_links ) :
				foreach ( $social_links as $social ) {
					if ( strtolower($social['title']) == 'facebook' ) $icon = '01-FB-top.png';
						else if ( strtolower($social['title']) == 'google+' ) $icon = '02-GP-top.png';
						else if ( strtolower($social['title']) == 'twitter' ) $icon = '03-TW-top.png';
						else if ( strtolower($social['title']) == 'youtube' ) $icon = '04-YT-top.png';
						else if ( strtolower($social['title']) == 'linkedin' ) $icon = '05-LN-top.png';

						echo '<a href="' . $social['social_links_href'] . '" target="_blank">';
						echo "<img src='";
						echo MONSARRAT_THEME_PATH;
						echo "/images/$icon'";
						echo ' alt="' . $social['title'] . '" title="' . $social['title'] . '"></a>';
					}
				else : ?>
					<a href="#"><img src="<?php echo MONSARRAT_THEME_PATH; ?>/images/01-FB-top.png" alt="Facebook" title="Facebook"></a>
					<a href="#"><img src="<?php echo MONSARRAT_THEME_PATH; ?>/images/02-GP-top.png" alt="Google+" title="Google+"></a>
					<a href="#"><img src="<?php echo MONSARRAT_THEME_PATH; ?>/images/03-TW-top.png" alt="Twitter" title="Twitter"></a>
					<a href="#"><img src="<?php echo MONSARRAT_THEME_PATH; ?>/images/04-YT-top.png" alt="Youtube" title="Youtube"></a>
				<?php endif ; ?>
				<input style="margin-left: 15px;" type="text" name="search" placeholder="Search...">
			</div>
		</div><!-- DIV con Social y Search FIN -->
	</div> <!-- Primera barra del tope FIN -->

	<div class="row hidden-xs" id="tope2"><!-- Segunda barra del tope  -->
	</div><!-- Segunda barra del tope FIN -->

<?php if ( is_front_page() ) : ?>
	<div class="row owl-carousel owl-theme" id="banner"><!-- BANNER -->
		<?php $banner_items = ot_get_option( 'main_banner' );
		if ( $banner_items ) :
			foreach ($banner_items as $item) :
				echo '<div class="main-banner">';
				echo '<img src="'.$item['image'].'">';
				echo '<span><h1>' . $item['title'] . '</h1>';
				echo $item['description'];
				echo '</span></div>';
			endforeach;
		else :
			echo '<img src="' . MONSARRAT_THEME_PATH . '/images/banner.jpg" class="img-responsive center-block" alt="Banner">';
		endif; ?>
	</div><!-- BANNER FIN -->
<?php endif; ?>

	<div class="row owl-carousel owl-theme" id="header"><!-- HEADER -->
		<?php $banner_subitems = ot_get_option( 'mini_slider' );
		if ( $banner_subitems ) :
			foreach ($banner_subitems as $item) :
				echo '<div class="mini-slide">';
				echo '<a href="' . $item['link'] .'"><img src="'.$item['image'].'">';
				echo '<h3>' . $item['title'] . '</h3></a>';
				echo '</div>';
			endforeach;
		else :
			echo '<img src="' . MONSARRAT_THEME_PATH . '/images/heading-holder.jpg" class="img-responsive center-block" alt="Heading">';
		endif; ?>
	</div><!-- HEADER FIN -->	
